package ru.lanit.ipr.ipushkarev.userservice.service.impl;

import ru.lanit.ipr.ipushkarev.userservice.dao.api.UserDao;
import ru.lanit.ipr.ipushkarev.userservice.model.domain.User;
import ru.lanit.ipr.ipushkarev.userservice.model.dto.request.CreateUserRequestDto;
import ru.lanit.ipr.ipushkarev.userservice.model.dto.response.CreateUserResponseDto;
import ru.lanit.ipr.ipushkarev.userservice.service.api.UserService;

import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
public class UserServiceImpl implements UserService {

    @Inject
    private UserDao userDao;

    @Override
    public CreateUserResponseDto createUser(CreateUserRequestDto createUserRequestDto) {
        User user = new User();
        user.setFirstName("First");
        user.setLastName("Last");
        user.setMiddleName("Middle");
        user.setAge(25);

        User user1 = userDao.save(user);

        User user2 = userDao.getById(user1.getUserId());

        return new CreateUserResponseDto();
    }
}
