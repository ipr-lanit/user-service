package ru.lanit.ipr.ipushkarev.userservice.ws.impl;

import ru.lanit.ipr.ipushkarev.userservice.model.dto.request.CreateUserRequestDto;
import ru.lanit.ipr.ipushkarev.userservice.model.dto.response.CreateUserResponseDto;
import ru.lanit.ipr.ipushkarev.userservice.service.api.UserService;
import ru.lanit.ipr.ipushkarev.userservice.ws.api.UserWebService;

import javax.inject.Inject;
import javax.jws.WebService;

@WebService
public class UserWebServiceImpl implements UserWebService {

    @Inject
    private UserService userService;

    @Override
    public CreateUserResponseDto createUser(CreateUserRequestDto createUserRequestDto) {
        return userService.createUser(createUserRequestDto);
    }
}
