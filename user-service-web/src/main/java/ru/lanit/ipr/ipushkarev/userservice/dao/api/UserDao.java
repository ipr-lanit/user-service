package ru.lanit.ipr.ipushkarev.userservice.dao.api;

import ru.lanit.ipr.ipushkarev.userservice.model.domain.User;

import javax.ejb.Local;

@Local
public interface UserDao {

    User getById(int id);

    User save(User user);

    User update(User user);

    int delete(User user);
}
