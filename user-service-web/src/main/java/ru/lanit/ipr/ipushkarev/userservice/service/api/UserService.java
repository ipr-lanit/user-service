package ru.lanit.ipr.ipushkarev.userservice.service.api;

import ru.lanit.ipr.ipushkarev.userservice.model.dto.request.CreateUserRequestDto;
import ru.lanit.ipr.ipushkarev.userservice.model.dto.response.CreateUserResponseDto;

import javax.ejb.Local;

@Local
public interface UserService {

    CreateUserResponseDto createUser(CreateUserRequestDto createUserRequestDto);
}
