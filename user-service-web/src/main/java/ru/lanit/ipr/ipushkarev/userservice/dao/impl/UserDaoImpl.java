package ru.lanit.ipr.ipushkarev.userservice.dao.impl;

import ru.lanit.ipr.ipushkarev.userservice.dao.api.UserDao;
import ru.lanit.ipr.ipushkarev.userservice.model.domain.User;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.PersistenceContext;

@Stateless
public class UserDaoImpl implements UserDao {

    @PersistenceContext(unitName = "persistenceUnitUserService")
    private EntityManager entityManager;

    @Override
    public User getById(int id) {
        return entityManager.find(User.class, id);
    }

    @Override
    public User save(User user) {
        entityManager.persist(user);
        return user;
    }

    @Override
    public User update(User user) {
        entityManager.refresh(user, LockModeType.OPTIMISTIC);
        return user;
    }

    @Override
    public int delete(User user) {
        entityManager.remove(user);
        return 1;
    }
}
