package ru.lanit.ipr.ipushkarev.userservice.ws.api;

import ru.lanit.ipr.ipushkarev.userservice.model.dto.request.CreateUserRequestDto;
import ru.lanit.ipr.ipushkarev.userservice.model.dto.response.CreateUserResponseDto;

import javax.jws.WebService;

@WebService
public interface UserWebService {

    CreateUserResponseDto createUser(CreateUserRequestDto createUserRequestDto);
}
